FROM python:3.11 as BUILDER


COPY . /src
WORKDIR /src

RUN poetry install
RUN poetry build --format=wheel


FROM python:3.11 as INSTALL

COPY --from=BUILDER /src/*.whl /package
WORKDIR /package
RUN pip install *.whl
RUN ansible-galaxy collection install community.general --upgrade
