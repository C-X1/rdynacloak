"""Configure Sphinx."""

import os
import sys
from typing import List

sys.path.insert(0, os.path.abspath(".."))

import rdynacloak  # noqa: E402, F401

# -- Project information -----------------------------------------------------

project = "rDynaCloak"
copyright = "2023, Christian Holl"  # noqa: F412, A001
author = "Christian Holl"

# The full version, including alpha/beta/rc tags
release = "1.0.0"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named "sphinx.ext.*") or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx.ext.coverage",
    "sphinx.ext.mathjax",
    "sphinx.ext.ifconfig",
    "sphinx.ext.viewcode",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns: List[str] = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = "sphinx_rtd_theme"
pygments_style = "sphinx"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# html_static_path = ["_static"]  # noqa: E800, E265

autodoc_default_options = {"members": True, "show-inheritance": True}

autosummary_generate = True

# html_logo = "A Poetry Project_logo.svg"  # noqa: E800, E265
html_theme_options = {
    # "logo_only": True,  # noqa: E800, E265
    "display_version": True,
}

todo_include_todos = True
