"""Make rdynacloak executable by `python -m rdynacloak`."""
from rdynacloak import rdynacloak

if __name__ == "__main__":  # pragma: no cover
    rdynacloak.main()
