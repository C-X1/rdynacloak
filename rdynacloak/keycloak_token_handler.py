"""Handle creation, renewal and auth data generation."""

import datetime

import requests
from tenacity import retry, stop_after_attempt, wait_random

from rdynacloak.config import RDynacloakConfiguration
from rdynacloak.logger import logger

logger_public = logger.bind(public=True)


class KeycloakTokenError(Exception):
    """Provide error for token handling."""


class KeycloakTokenHandler(object):
    """Handle auth tokens."""

    def __init__(self, config: RDynacloakConfiguration) -> None:
        """Initialize class.

        :param config: App configuration
        """
        self._config = config
        self._token_data = None  # Storage for current access token

    def insert_auth_information(self, config: dict) -> None:
        """Insert auth information to configuration dictionary.

        :param config: The struct to insert the auth information into.
        """
        config["auth_keycloak_url"] = self._config.keycloak_url
        config["validate_certs"] = self._config.verify
        if self._config.auth_client_secret:
            config["auth_client_secret"] = self._config.auth_client_secret

        config["token"] = self.get_access_token()

    def get_access_token(self) -> str:
        """Return valid access token.

        :returns: Access token
        """
        create_token = self._token_data is None
        renew_token = not create_token

        if create_token or renew_token:
            self.create_or_renew_token()

        return self._token_data["token_response"]["access_token"]

    @retry(reraise=True, stop=stop_after_attempt(3), wait=wait_random(2, 10))
    def create_or_renew_token(self) -> None:
        """Create or refresh authentication token.

        :raises KeycloakTokenError: if token operation failed.
        """
        if self._token_data:
            payload = {
                "grant_type": "refresh_token",
                "client_id": self._config.auth_client_id,
                "refresh_token": self._token_data["token_response"]["refresh_token"],
            }
        else:
            payload = {
                "grant_type": "password",
                "client_id": self._config.auth_client_id,
                "username": self._config.auth_username,
                "password": self._config.auth_password,
            }

        if self._config.auth_client_secret:
            payload["client_secret"] = self._config.auth_client_secret

        try:  # noqa: WPS229
            response = requests.post(
                "{}/realms/{}/protocol/openid-connect/token".format(
                    self._config.keycloak_url, self._config.auth_realm
                ),
                data=payload,
                verify=self._config.verify,
                timeout=self._config.timeout,
            )
            response.raise_for_status()
            token_data = response.json()
            self._token_data = {
                "renew_after": (
                    datetime.datetime.now()
                    + datetime.timedelta(seconds=token_data["expires_in"] / 2)
                ),
                "token_response": token_data,
            }

        except requests.exceptions.RequestException as exception:
            op_description = "creating"
            if self._token_data:
                op_description = "renewing"

            message = "{} on {} token.".format(type(exception), op_description)

            self._token_data = None
            logger_public.error(message)
            raise KeycloakTokenError(message)
