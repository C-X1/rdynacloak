"""Watch directory for changes to trigger update."""

import threading
from enum import Enum
from typing import Dict

from watchdog.events import FileSystemEvent, FileSystemEventHandler
from watchdog.observers import Observer


class UpdateMode(Enum):
    """Supply update methods.

    NONE: No update required
    UPDATE: Simple update required
    REINITIALIZE: Complete system reset with reinitialization required
    """

    NONE = (0,)  # noqa: WPS115
    UPDATE = (1,)  # noqa: WPS115
    REINITIALIZE = 2  # noqa: WPS115


class DirectoryChangeHandler(FileSystemEventHandler):
    """Handle events on directory changes."""

    def __init__(self) -> None:
        """Initialize counters."""
        self._created = 0
        self._deleted = 0
        self._modified = 0
        self._moved = 0
        self._lock = threading.Lock()

    def event_relevant(self, event: FileSystemEvent) -> bool:
        """Check event for relevance.

        :param event: The event
        :returns: true if event originates from a yaml file.
        """
        return not event.is_directory and event.src_path.lower().endswith(".yaml")

    def on_created(self, event: FileSystemEvent) -> None:
        """Count file created events.

        :param event: The file system event
        """
        if self.event_relevant(event):
            with self._lock:
                self._created += 1

    def on_moved(self, event: FileSystemEvent) -> None:
        """Count moved events.

        :param event: The file system event
        """
        if self.event_relevant(event):
            with self._lock:
                self._moved += 1

    def on_deleted(self, event: FileSystemEvent) -> None:
        """Count file deleted events.

        :param event: The file system event
        """
        if self.event_relevant(event):
            with self._lock:
                self._deleted += 1

    def on_modified(self, event: FileSystemEvent) -> None:
        """Count file created event.

        :param event: The file system event
        """
        if self.event_relevant(event):
            with self._lock:
                self._modified += 1

    def counts(self) -> Dict[str, int]:
        """Return current counts.

        :returns: Dictionary with counts of changes:
        """
        with self._lock:
            return {
                "created": self._created,
                "deleted": self._deleted,
                "moved": self._moved,
                "modified": self._modified,
            }

    def count_and_reset(self) -> Dict[str, int]:
        """Get current counts and reset.

        :returns: Dictionary with counts of changes
        """
        with self._lock:
            counts = {
                "created": self._created,
                "deleted": self._deleted,
                "moved": self._moved,
                "modified": self._modified,
            }

            self._created = 0
            self._deleted = 0
            self._moved = 0
            self._modified = 0

            return counts


class DirectoryWatcher(object):
    """Watch directories for changes and count them."""

    def __init__(self, directory: str) -> None:
        """Count directory changes.

        :param directory: The directory to be watched for changes
        """
        self._change_handler = DirectoryChangeHandler()
        self._observer = Observer()
        self._observer.schedule(self._change_handler, directory, recursive=False)

    def obtain_counts_and_reset(self) -> Dict[str, int]:
        """Obtain counts and reset counters.

        :returns: Dictionary with counted changes.
        """
        return self._change_handler.count_and_reset()

    def obtain_counts(self) -> Dict[str, int]:
        """Obtain counts and reset counters.

        :returns: Dictionary with counted changes.
        """
        return self._change_handler.counts()

    def start(self) -> None:
        """Start watching directory."""
        self._observer.start()

    def stop(self) -> None:
        """Stop watching directory."""
        self._observer.stop()
        self._observer.join()
