"""Configure rdynacloak."""

import os
from typing import Optional, Union

import yaml
from pydantic import BaseModel

CSS_DEFAULT_FILE = os.path.join(os.path.dirname(__file__), "page.css")


class RDynacloakConfiguration(BaseModel):
    """Hold dynacloak configuration."""

    # Auth information.
    keycloak_url: Optional[str] = "https://localhost:8080"
    auth_username: Optional[str] = "user"
    auth_password: Optional[str] = "password"
    auth_realm: Optional[str] = "master"
    auth_client_id: Optional[str] = "admin-cli"
    auth_client_secret: Optional[Union[str, None]] = None

    # Config directory
    config_dir: Optional[str] = "."

    # Managed realm
    realm: Optional[str] = "master"

    # Requests
    verify: Optional[bool] = True  # Check certificates
    timeout: Optional[int] = 30  # Timeout of connections

    css_file: Optional[str] = CSS_DEFAULT_FILE


def read_configuration() -> RDynacloakConfiguration:
    """Read configuration from config file.

    :returns: configuration for rdynacloak
    """
    path = os.environ.get("RDYNACLOAK_CONFIG_PATH")

    if path:
        return RDynacloakConfiguration(
            **yaml.safe_load(open(path, "r").read())  # noqa: WPS515
        )

    return RDynacloakConfiguration()
