var old_data = {};

function create_div(div_id, div_class) {
  let div_element =  document.createElement("div");
  div_element.id = div_id;
  div_element.className = div_class;
  return div_element;
}

function delete_removed_services(new_data) {
  const old_keys = Object.keys(old_data);
  const new_keys = Object.keys(new_data);
  const delete_entries = old_keys.filter(
    item => !new_keys.includes(item)
  );

  delete_entries.forEach(entry => {
    const element = document.getElementById(
      `${entry}_card`
    );
    if (element) {
      element.remove();
    }
  });
}

function create_services(new_data) {
  const new_keys = Object.keys(new_data).sort();
  const old_keys = Object.keys(old_data);
  create_entries = new_keys.filter(
    item => !old_keys.includes(item)
  );

  create_entries.forEach(entry => {
    index = new_keys.indexOf(entry);
    var before = null;
    if ((index + 1) < new_keys.length) {
      before = new_keys[index + 1];
    }
    create_service(entry, before);
  });
}

function create_service(entry, before) {
  let div_content = document.getElementById("status_box");
  const card_id = `${entry}_card`;
  const state_id = `${entry}_state`;
  const label_id = `${entry}_label`;
  const desc_id = `${entry}_desc`;

  div_card = create_div(card_id, "list_entry");
  div_card.className = "list_entry";
  div_card.appendChild(create_div(state_id, "state"));
  label_div = create_div(label_id, "label");
  label_div.innerHTML = entry;
  div_card.appendChild(label_div);

  if (before) {
    before_element = document.getElementById(`${before}_card`);
    div_content.insertBefore(div_card, before_element);
  } else {
    div_content.append(div_card);
  }
}

function update_services(new_data) {
  Object.keys(new_data).forEach(entry => {
    state_div = document.getElementById(`${entry}_state`);
    if(state_div) {
      state_div.innerHTML = new_data[entry] ? "&#x1F7E2;" : "&#x1F534;";
    }
  });
}


function update_entries(data) {
  const new_data = JSON.parse(data);
  delete_removed_services(new_data);
  create_services(new_data);
  update_services(new_data);
  old_data = new_data;
}

function display_process(data) {
  let pdata = JSON.parse(data);
  let bar = document.getElementById('progress_bar');
  const progress = (pdata.progress >= 0) ? pdata.progress : 0;


  if (progress != 0 && progress != 100) {
    bar.classList.add('visible');
    bar.classList.remove('hidden');
  } else {
    bar.classList.remove('visible');
    bar.classList.add('hidden');
  }


  let label = document.getElementById("progress_label");
  label.innerHTML = (pdata.progress >= 0) ? `${progress}&nbsp;&#37;` : "- n/a -";
  document.getElementById('progress_fill').style.width = `${progress}%`;

}

function show_error(id, error) {
  var element = document.getElementById(id);
  console.log("....");
  element.innerHTML = error;
  element.classList.add('visible');
  element.classList.remove('hidden');
}

function hide_error(id) {
  var element = document.getElementById(id);
  element.classList.remove('visible');
  element.classList.add('hidden');
}

function status() {
  fetch("/services")
    .then(response => response.text())
    .then(data => {
      update_entries(data);
      hide_error("status_error");
    })
    .catch(error => {
      console.log(error);
      show_error("status_error", error);
    });

  fetch("/progress")
    .then(response => response.text())
    .then(data => {
      display_process(data);
      hide_error("progress_error");
    })
    .catch(error => {
      console.log(error);
      show_error("progress_error", error);
    });
}

function update() {
  status();
}
update();
setInterval(update, 1000);

var log_source;
setup_event_source();


function setup_event_source() {
  log_source = new EventSource("/logs");
  log_source.onerror = (event) => {
    show_error("log_error", "Connection Problem");
    log_source.close();
    setTimeout(setup_event_source, 10000);
  }
  log_source.onmessage = function(event) {
    hide_error("log_error");
    var symbols = {
      "debug": "🔍 ",
      "warning": "⚠  ",
      "error": "✗ "
    };
    var data = JSON.parse(event.data);

    const autoscroll = document.getElementById("autoscroll_enable");
    const log_output = document.getElementById("log_output");
    const entry = document.createElement("div");

    entry.classList.add("log_entry", data.level);
    entry.textContent =  data.event;

    if(data.level in symbols) {
      entry.textContent =  symbols[data.level] + data.event;
    }

    log_output.appendChild(entry);

    if (autoscroll.checked) {
      log_output.scrollTop = log_output.scrollHeight;
    }
  };

}
