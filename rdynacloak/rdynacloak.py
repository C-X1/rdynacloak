"""Construct and run main application."""

import sys

import structlog
import typer
import uvicorn

from rdynacloak.api import api
from rdynacloak.api_context import context
from rdynacloak.config import read_configuration

app = typer.Typer()
HOST_OPTION = typer.Option(default="0.0.0.0", help="The host binding.")  # noqa: S104
PORT_OPTION = typer.Option(
    default=8000, help="The port to listen for connections."  # noqa: WPS432
)


@app.command()
def serve(
    host: str = HOST_OPTION,
    port: int = PORT_OPTION,
) -> None:
    """Start configuration service.

    :param host: The host binding.
    :param port: The port to listen for new connections.
    """
    # context.configuration TODO

    context.configuration = read_configuration()
    uvicorn.run(api, host=host, port=port)


@app.callback(no_args_is_help=True)
def callback() -> None:
    """Show help when called with no arguments."""


def main() -> None:
    """Construct typer_click interface."""
    log = structlog.get_logger()
    typer_click = typer.main.get_command(app)
    try:
        typer_click()
    except Exception as exception:
        log.fatal("Caught unhandled exception")
        log.exception(exception)
        sys.exit(1)


if __name__ == "__main__":  # pragma: no cover
    main()
