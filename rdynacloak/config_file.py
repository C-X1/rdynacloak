"""Create dynamic config file class."""

import os
from typing import Dict, Optional, Union

import yaml
from pydantic import BaseModel, ConfigDict, create_model

from rdynacloak.ansible_executor import list_ansible_keycloak_modules

ANNOTATIONS = "__annotations__"
CUSTOM_CONFIGURATIONS = tuple()  # noqa: C408
SORT_CONFIG = (
    "authentication",
    "realm",
    "group",
    "user",
    "identity_provider",
    "user_federation",
    "client",
    "role",
    "client_rolemapping",
    "user_rolemapping"
)


class ConfigurationApplicationError(Exception):
    """Supply error when configuration application failed."""


def list_configurations() -> list:
    """List configuration entries in file.

    :raises RuntimeError: if expected configurations are not available
    :returns: List of all different configurations possible.
    """
    available_configurations = (
        list_ansible_keycloak_modules() + list(CUSTOM_CONFIGURATIONS)
    )

    for config in SORT_CONFIG:
        if config not in available_configurations:
            raise RuntimeError(f"Configurations missing: {config}")

        available_configurations.remove(config)

    return list(SORT_CONFIG) + available_configurations


def create_config_basemodel_by_types(name: str) -> BaseModel:
    """Create dynamic entries for module configurations.

    :param name: Model name
    :returns: Class model
    """
    model_members = {}

    for module in list_configurations():
        model_members[f"{module}s"] = (list, [])

    return create_model(name, **model_members)


ConfigFileDynamic = create_config_basemodel_by_types("ConfigFileDynamic")


class ConfigFile(ConfigFileDynamic):
    """Specify static configuration file fields."""

    model_config = ConfigDict(extra="forbid")

    service_id: Optional[str] = "general"

    @staticmethod
    def read_config_file(filepath: str) -> "ConfigFile":
        """Try to load the array from a yaml config file.

        :param filepath: The path of the file
        :returns: Config file representation or None on error.
        """
        with open(filepath, "r") as config_file:
            return ConfigFile(**yaml.safe_load(config_file))

    @staticmethod
    def load_from_directory(  # noqa: WPS210
        directory: str,
    ) -> Dict[str, Union["ConfigFile", Exception]]:
        """Load all configurations from directory.

        :param directory: The directory to look for files (non recursively)
        :returns: Dictionary with path as key and the
                  value is the config file or an occured
                  exception.
        """
        config_files = {}

        for file_path in os.listdir(directory):
            config_file_path = os.path.join(directory, file_path)
            ext_yaml = config_file_path.lower().endswith(".yaml")
            ext_yml = config_file_path.lower().endswith(".yml")

            config_file: Union[None, Exception, ConfigFile] = None
            if ext_yaml or ext_yml:
                try:
                    config_file = ConfigFile.read_config_file(config_file_path)
                except Exception as error:
                    config_file = error

                config_files[file_path] = config_file

        return config_files
