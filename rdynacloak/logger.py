"""Logger setup for logging to console and web UI."""

import json
from logging import Logger
from threading import Lock
from typing import List

import structlog


class LogSubscriber(object):
    """Log Subscriber for web output."""

    def __init__(self, inital_events: List[str]) -> None:
        """Init class.

        :param inital_events: Initial event list of the subscriber
        """
        self.lock = Lock()
        self.event_queue = inital_events

    def event(self, event_json: str) -> None:
        """Process event from Log Processor.

        :param event_json: The json represenation of the event.
        """
        with self.lock:
            self.event_queue.append(event_json)

    def events(self) -> List[str]:
        """Return string of all current events.

        :returns: JSON string of all current events
        """
        with self.lock:
            current_events = self.event_queue
            self.event_queue = []
            return current_events


class LogProcessor(object):
    """Log processor for web output."""

    def __init__(self) -> None:
        """Init class."""
        self.events: List[str] = []
        self.lock = Lock()
        self.subscribers: List[LogSubscriber] = []

    def __call__(self, _logger: Logger, _method_name: str, event_dict: dict) -> dict:
        """Log event for web UI.

        :param _logger: The logger
        :param _method_name: The logging method name
        :param event_dict: The dict containing the event info
        :returns: Event dictionary
        """
        with self.lock:
            if event_dict.get("public"):
                json_dict = json.dumps(event_dict)
                if event_dict.get("start"):
                    self.events = []
                self.events.append(json_dict)
                for subscriber in self.subscribers:
                    subscriber.event(json_dict)
        return event_dict

    def subscribe(self) -> LogSubscriber:
        """Subscribe log events.

        :returns: A new log subscriber object
        """
        with self.lock:
            log_subscriber = LogSubscriber(self.events)
            self.subscribers.append(log_subscriber)
            return log_subscriber

    def unsubscribe(self, subscriber: LogSubscriber) -> None:
        """Unsubscribe log events.

        :param subscriber: The subscriber to remove
        """
        with self.lock:
            self.subscribers.remove(subscriber)


string_io_log_processor = LogProcessor()
structlog.configure_once(
    processors=[
        structlog.contextvars.merge_contextvars,
        structlog.processors.add_log_level,
        structlog.processors.StackInfoRenderer(),
        structlog.dev.set_exc_info,
        structlog.processors.TimeStamper(
            fmt="%Y-%m-%d %H:%M:%S", utc=False  # noqa: WPS323
        ),
        string_io_log_processor,  # type: ignore
        structlog.dev.ConsoleRenderer(),
    ],
    wrapper_class=None,
    context_class=None,
    logger_factory=structlog.stdlib.LoggerFactory(),
    cache_logger_on_first_use=None,
)

logger = structlog.get_logger()
logger_public = logger.bind(public=True)
logger_start = logger_public.bind(start=True)
