"""Apply configurations to keycloak."""


import threading
import time
from enum import Enum
from typing import Dict

from tenacity import retry, retry_if_not_exception_type, stop_after_attempt, wait_random

from rdynacloak.ansible_executor import AnsibleExecutor, list_ansible_keycloak_modules
from rdynacloak.config import RDynacloakConfiguration
from rdynacloak.config_file import (
    ConfigFile,
    ConfigurationApplicationError,
    list_configurations,
)
from rdynacloak.directory_watch import DirectoryWatcher
from rdynacloak.keycloak_token_handler import KeycloakTokenHandler
from rdynacloak.logger import logger, logger_public, logger_start

CREATED = "created"
MODIFIED = "modified"
DELETED = "deleted"


class UpdateType(str, Enum):  # noqa: WPS600
    """Specify update type."""

    NO_UPDATE = "no update"  # noqa: WPS115
    UPDATE = "update"  # noqa: WPS115
    UPDATE_WITH_RESET = "update with reset"  # noqa: WPS115


class KeycloakConfigApplicator(object):
    """Using Ansibles Keycloak modules to apply configuration."""

    def __init__(self, config: RDynacloakConfiguration) -> None:  # noqa: S107, WPS211
        """Init class.

        :param config: App configuration
        """
        self._config = config
        self._token_handler = KeycloakTokenHandler(config)

        self._lock = threading.Lock()  # Thread lock
        self._service_status: Dict[str, bool] = {}  # Status of services

        # Progressbar
        self._applied_count = 0  # Number of applied configurations
        self._overall_count = 0  # Overall number of configurations to apply
        self._service_status_update: Dict[str, bool] = {}  # Status Storage current run
        self._running = False

        self.directory_watcher = DirectoryWatcher(config.config_dir)

    def start(self) -> None:
        """Start thread."""
        self._running = True
        self.thread = threading.Thread(target=self.run, args=())
        self.thread.daemon = True  # Daemonize thread
        self.thread.start()  # Start the execution

    def stop(self) -> None:
        """End thread."""
        self._running = False
        logger_public.info("Stopping configuration applicator...")
        self.thread.join()

    def startup(self) -> None:
        """Execute on startup."""
        logger_public.info("ConfigApplicationHandler startup")

        self.counts = self.directory_watcher.obtain_counts()
        self.directory_watcher.start()

    def shutdown(self) -> None:
        """Execute on shtdown."""
        logger_public.info("Config applicator exit")
        self.directory_watcher.stop()

    def update_type(self) -> UpdateType:
        """Determine necessary update type.

        :returns: Type of update to perform
        """
        if self.init:
            init_update = UpdateType.UPDATE_WITH_RESET
            if self._config.realm == "master":
                init_update = UpdateType.UPDATE
            return init_update

        counts = self.directory_watcher.obtain_counts()
        difference = {}
        for key in counts.keys():
            difference[key] = counts[key] - self.counts[key]
        self.counts = counts

        if difference[DELETED]:
            return UpdateType.UPDATE_WITH_RESET

        elif difference[MODIFIED] - difference[CREATED]:
            return UpdateType.UPDATE_WITH_RESET

        elif difference[CREATED] or self.retry:
            self.retry = False
            return UpdateType.UPDATE

        return UpdateType.NO_UPDATE

    def handle_config_updates(self) -> None:
        """Handle configurations."""
        update = self.update_type()

        if update == UpdateType.NO_UPDATE:
            return

        logger_start.info(f"Performing {update}")

        self.apply_configs(update == UpdateType.UPDATE_WITH_RESET)

        self.init = False
        if self.retry:
            logger_public.warning("One or more unsuccessful updates, retrying...")
        else:
            logger_public.info("✅ Update successful!")


    def run(self) -> None:
        """Handle task for configuration application."""
        self.init = True
        self.startup()
        while self._running:
            time.sleep(10)
            try:
                self.handle_config_updates()
            except Exception:
                self.retry = True
                logger_public.error("Error, Retrying...")
        self.shutdown()

    def is_service_ready(self, service_id: str) -> bool:
        """Get ready state of service.

        :param service_id: The service id
        :returns: True if service was found and applied successfully, false otherwise
        """
        with self._lock:
            general = bool(self._service_status.get("general", True))
            service = bool(self._service_status.get(service_id))
            return general and service

    def services_status(self) -> Dict[str, bool]:
        """Return status of last finished update.

        :returns: Dictionary with service_id -> no_errors
        """
        return self._service_status

    def services_update_status(self) -> Dict[str, bool]:
        """Return latest update status, also if running.

        :returns: Dictionary with service_id -> no_errors
        """
        return self._service_status_update

    def delete_pod(self) -> None:
        """Delete pod."""

    def recreate_realm(self) -> None:
        """Delete current realm."""
        logger_public.info("Recreating realm {}".format(self._config.realm))
        config = {
            "id": self._config.realm,
            "enabled": True,
            "realm": self._config.realm,
            "state": "absent",
        }
        self._token_handler.insert_auth_information(config)

        for _ in range(2):
            self.apply_ansible_config("realm", config)
            config["state"] = "present"

    def init_update_status(self, config_files: Dict[str, ConfigFile]) -> None:
        """Count the amount of needed module calls and initialize progessbar and status.

        This counts the amount of configurations needed to be made,
        and resets the applied counter to zero, and the overall count
        to the number of configurations.

        :param config_files: Dictionary of configuration files.
        """
        self._service_status_update = {}
        count = 0
        with self._lock:
            self._applied_count = 0  # Number of applied configurations
            for config in config_files.values():
                self._service_status_update[config.service_id] = True
                for config_name in list_configurations():  # noqa: WPS519
                    count += len(getattr(config, f"{config_name}s"))

            self._overall_count = count  # Overall number of configurations to apply

    def reset_config(self) -> None:
        """Reset configuration."""
        with self._lock:
            self._service_status = {}

        if self._config.realm == "master":
            self.delete_pod()
        else:
            self.recreate_realm()

    @retry(reraise=True, stop=stop_after_attempt(3), wait=wait_random(2, 10))
    def apply_ansible_config(self, config_name: str, config: dict) -> None:
        """Apply a configuration with an ansible keycloak module.

        :param config_name: The _singular_ form of the configuration name.
        :param config: The configuration.
        :raises ConfigurationApplicationError: if failed to apply
        """
        exec_result = AnsibleExecutor().run_ansible_module(
            (
                "ansible_collections.community.general"
                f".plugins.modules.keycloak_{config_name}"
            ),
            config,
        )
        if exec_result.get("failed", False):
            logger.error(exec_result)
            raise ConfigurationApplicationError("Configuration failed.")

    @retry(
        retry=retry_if_not_exception_type(NotImplementedError),
        reraise=True,
        stop=stop_after_attempt(3),
        wait=wait_random(2, 10),
    )
    def apply_custom_config(self, config_name: str, config: dict) -> None:
        """Apply a configuration with an custom function in this class.

        :param config_name: The _singular_ form of the configuration name.
        :param config: The configuration
        :raises NotImplementedError: if functio to apply config not found.
        """
        try:
            applicator = getattr(self, f"apply_custom_{config_name}")
        except AttributeError:
            raise NotImplementedError(
                "No function implemented for config f{config_name}."
            )
        applicator(config)

    def progress(self) -> int:
        """Return progress value.

        :returns: Progress in percent of the current update. -1 on never run.
        """
        with self._lock:
            if self._overall_count:
                return int(self._applied_count * 100 / self._overall_count)
        return -1

    def apply_config_from_file(self, config_name: str, config_file: ConfigFile) -> None:
        """Apply a configuration array from a configuration file.

        :param config_name: The _singular_ form of the configuration name.
        :param config_file: The ConfigFile object of the file.
        """
        configs = getattr(config_file, f"{config_name}s")
        applicator = self.apply_custom_config
        if config_name in list_ansible_keycloak_modules():
            applicator = self.apply_ansible_config

        implemented = True
        for index, config in enumerate(configs):

            if implemented:
                self._token_handler.insert_auth_information(config)
                config["realm"] = self._config.realm
                try:
                    applicator(config_name, config)

                except NotImplementedError:
                    logger_public.error(f"Not implemented configuration: {config_name}")
                    self._service_status_update[config_file.service_id] = False
                    implemented = False

                except Exception:
                    logger_public.error(
                        f"Error for configuration {config_name}({index})"
                        f" for id {config_file.service_id}"
                    )
                    self.retry = True
                    self._service_status_update[config_file.service_id] = False

            with self._lock:
                self._applied_count += 1

        with self._lock:
            self._service_status = self._service_status_update

    def load_configuration_files(self) -> Dict[str, ConfigFile]:
        """Load configurations and log errors.

        :returns: Dict with configuration files.
        """
        config_files_cleared = {}
        config_files = ConfigFile.load_from_directory(self._config.config_dir)

        for file_path, config_file in config_files.items():
            if isinstance(config_file, Exception):
                error = type(config_file)
                logger_public.error(f"{error} while parsing {file_path}.")
            else:
                config_files_cleared[file_path] = config_file

        return config_files_cleared

    def apply_configs(self, reset: bool) -> None:  # noqa: WPS231
        """Apply existing configurations to keycloak.

        :param reset: Execute full reset if true -> delete pod (master) or realm
        """
        config_files = self.load_configuration_files()
        self.init_update_status(config_files)

        if reset:
            self.reset_config()

        for config_name in list_configurations():
            for file_path, config_file in config_files.items():
                if getattr(config_file, f"{config_name}s"):
                    logger_public.info(f"Now applying {config_name}s from {file_path}")
                    self.apply_config_from_file(config_name, config_file)

        with self._lock:
            self._service_status = self._service_status_update
