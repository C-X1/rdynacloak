"""Define api context."""

from typing import Union

from rdynacloak.config import RDynacloakConfiguration
from rdynacloak.directory_watch import DirectoryWatcher
from rdynacloak.keycloak_config_applicator import KeycloakConfigApplicator


class ApiContext(object):
    """Contain the API Context."""

    running: bool = False
    directory_watcher: Union[DirectoryWatcher, None] = None
    config_applicator: Union[KeycloakConfigApplicator, None] = None
    configuration: Union[RDynacloakConfiguration, None] = None


context = ApiContext()
