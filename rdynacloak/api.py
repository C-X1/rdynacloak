"""Implement web api for kubernetes probes."""

import asyncio
import os
from enum import Enum
from typing import Dict

from fastapi import FastAPI, Path, status
from fastapi.responses import HTMLResponse
from pydantic import BaseModel
from sse_starlette.sse import EventSourceResponse

from rdynacloak.api_context import context
from rdynacloak.keycloak_config_applicator import KeycloakConfigApplicator
from rdynacloak.keycloak_config_applicator_dumy import KeycloakConfigApplicatorDummy
from rdynacloak.logger import string_io_log_processor

SLEEP_BETW_EVENTS = 0.2
FILE_DIRECTORY = os.path.dirname(__file__)
api = FastAPI()


@api.on_event("startup")
async def startup_event() -> None:
    """Start background tasks on FastAPI startup."""
    context.config_applicator = KeycloakConfigApplicator(context.configuration)

    if os.environ.get("DUMMY"):
        context.config_applicator = KeycloakConfigApplicatorDummy(context.configuration)

    context.config_applicator.start()


@api.on_event("shutdown")
async def shutdown_event() -> None:
    """Execute on FastAPI shutdown."""
    context.config_applicator.stop()


class StatusResponse(BaseModel):
    """Implement Status response."""

    description: str = "not-set"
    is_ready: bool = False


class ProgressResponse(BaseModel):
    """Implement Progress resonse."""

    progress: int
    status: Dict[str, bool]


class StatusID(str, Enum):  # noqa: WPS600
    """Provide enum for query parameter status type."""

    STARTUP = "startup"
    LIVE = "live"
    READY = "ready"


@api.get("/", response_class=HTMLResponse)
async def web_root() -> HTMLResponse:
    """Provide web root page for human users.

    :returns: Webpage for human users.
    """
    css = open(context.configuration.css_file, "r").read()  # noqa: WPS515
    javascript = open(  # noqa: WPS515
        os.path.join(FILE_DIRECTORY, "page.js"), "r"
    ).read()
    html = open(os.path.join(FILE_DIRECTORY, "page.html"), "r").read()  # noqa: WPS515

    page_content = html.format(css=css, javascript=javascript)
    return HTMLResponse(content=page_content, status_code=status.HTTP_200_OK)


STATUS_ID_PATH = Path(title="The ID of the keycloak status to get")


@api.get("/keycloak/{status_id}", response_model=StatusResponse)
async def keycloak(status_id: StatusID = STATUS_ID_PATH) -> StatusResponse:
    """Supply managed keycloak status.

    :param status_id: The ID of the status to obtained.
    :returns: startup status.
    """
    return StatusResponse()


SERVICE_ID_PATH = Path(title="The readyness of given service")


@api.get("/service/{service_id}", response_model=StatusResponse)
async def service(service_id: str = SERVICE_ID_PATH) -> StatusResponse:
    """Supply service_id status.

    :param service_id: The ID of the service.
    :returns: service status.
    """
    return StatusResponse(
        description=service_id,
        is_ready=context.config_applicator.is_service_ready(service_id),
    )


@api.get("/services", response_model=Dict[str, bool])
async def services() -> Dict[str, bool]:
    """Return services status.

    :returns: services status.
    """
    return context.config_applicator.services_status()


@api.get("/progress", response_model=ProgressResponse)
async def progress() -> ProgressResponse:
    """Return progress of current configuration application.

    :returns: Configuration progress in percent.
    """
    return ProgressResponse(
        progress=context.config_applicator.progress(),
        status=context.config_applicator.services_update_status(),
    )


async def event_publisher() -> None:
    """Publish logging events.

    :yields: Logging events
    :raises error: Reraises asyncio.CancelledError
    """
    subscriber = string_io_log_processor.subscribe()

    try:
        while True:  # noqa: WPS457
            events = subscriber.events()
            for event in events:
                yield event
            await asyncio.sleep(SLEEP_BETW_EVENTS)
    except asyncio.CancelledError as error:
        string_io_log_processor.unsubscribe(subscriber)
        raise error


@api.get("/logs")
async def logs() -> EventSourceResponse:
    """Return progress of current configuration application.

    :returns: Configuration progress in percent.
    """
    return EventSourceResponse(event_publisher())
