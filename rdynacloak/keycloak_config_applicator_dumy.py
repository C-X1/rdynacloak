"""Dummy for websurface development."""


from datetime import datetime
from typing import Dict

from rdynacloak.keycloak_config_applicator import KeycloakConfigApplicator
from rdynacloak.logger import logger_public, logger_start

DUMMY_MODE_WARNING = "RUNNING IN DUMMY MODE!"


class KeycloakConfigApplicatorDummy(KeycloakConfigApplicator):
    """Dummy class for web interface development."""

    def start(self) -> None:
        """Do nothing except log."""
        logger_public.warning(DUMMY_MODE_WARNING)

    def stop(self) -> None:
        """Do nothing."""
        logger_public.warning("Exiting.")

    def progress(self) -> int:
        """Return a bogues progress value.

        Depends on the second of the minute.

        :returns: Bogus progress value.
        """
        current_time = datetime.now()
        process = int((current_time.second % 10) * 10)
        if process == 90:  # noqa: WPS432
            process = 100

        if process == 10:
            logger_start.info("Starting new update.")

        logger_public.info(f"Dummy progress {process}")

        if current_time.second == 30:  # noqa: WPS432
            logger_public.error("A test error")

        if current_time.second == 40:  # noqa: WPS432
            logger_public.warning("Short warning")
            logger_public.warning(
                "Very long warning text"
                " to see whats happening if it is"
                " a bit longer..."
            )

        if current_time.second == 50:  # noqa: WPS432
            logger_public.fatal("A fatal text")
            logger_public.debug("A debug text")

        return process

    def services_status(self) -> Dict[str, bool]:
        """Return a bogus update status.

        :returns: A bogus status fo display on the webpage.
        """
        current_time = datetime.now()
        five_second_bool = (current_time.second % 10) > 4
        return {
            "general": five_second_bool,
            "1_Good service config": True,
            "2_Good / Bad for 5 secs": five_second_bool,
            "3_Bad service config": False,
        }

    def services_update_statues(self) -> Dict[str, bool]:
        """Return a bogus status.

        :returns: same bogus status as service_status
        """
        return self.service_status()
