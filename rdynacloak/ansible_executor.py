"""Execute keycloak ansible modules."""

import contextlib
import importlib
import io
import json
import pkgutil
from typing import Any, Dict, List

from ansible.module_utils import basic  # type: ignore
from ansible.module_utils.common import text  # type: ignore

USER_FEDERATION = "user_federation"


def list_ansible_keycloak_modules() -> List[str]:  # noqa: WPS605
    """List all keycloak modules available in Ansibles community.general.

    :returns: List of keycloak modules available without "keycloak_" prefix
    """
    prefix = "keycloak_"

    general_modules = importlib.import_module(
        "ansible_collections.community.general.plugins.modules"
    )

    keycloak_modules = [
        module.name[len(prefix) :]
        for module in pkgutil.iter_modules(general_modules.__path__)
        if module.name.startswith(prefix)
    ]

    keycloak_modules.remove(USER_FEDERATION)
    keycloak_modules = [USER_FEDERATION] + keycloak_modules

    return keycloak_modules  # noqa:  WPS331


KEYCLOAK_MODULES_NAME_PREFIX = (
    "ansible_collections.community.general.plugins.modules.keycloak_"
)


class AnsibleExecutor(object):
    """Execute ansible modules."""

    def run_ansible_module(self, module_name: str, args: dict) -> dict:
        """Run a keycloak module.

        :param module_name: Keycloak module name
        :param args: Keycloak module settings
        :returns: Ansible result read from stdout as JSON dict.
        """
        self._set_ansible_module_args(args)
        module = importlib.import_module(module_name)

        captured_stdout = io.StringIO()

        try:
            with contextlib.redirect_stdout(captured_stdout):
                module.main()
        except SystemExit:
            # Do nothing.
            pass  # noqa: WPS420

        return json.loads(captured_stdout.getvalue())

    def _set_ansible_module_args(self, args: Dict[Any, Any]) -> None:
        """Set the module args for executing the current model.

        :param args: The dictionary with the args for the module
        """
        args["_ansible_keep_remote_files"] = False

        if "_ansible_remote_tmp" not in args:
            args["_ansible_remote_tmp"] = "/tmp"  # noqa: S108

        ansible_args = {"ANSIBLE_MODULE_ARGS": args}

        args_json = json.dumps(ansible_args)
        basic._ANSIBLE_ARGS = text.converters.to_bytes(args_json)  # noqa: WPS437
