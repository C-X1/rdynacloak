#!/bin/bash


SCRIPT_PATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
export RDYNACLOAK_CONFIG_PATH=$SCRIPT_PATH/dynacloak.yaml

RELEASE_NAME=keycloak
NAMESPACE=dev



cat << EOF > "$RDYNACLOAK_CONFIG_PATH"
---
keycloak_url: "https://$(kubectl get helmreleases.helm.toolkit.fluxcd.io -n dev keycloak -o json | jq -r '.spec.values.ingress."hostname"'
)"
auth_username: user
auth_password: "$(kubectl get secrets -n $NAMESPACE $RELEASE_NAME -ojson | jq -r '.data."admin-password"' | base64 -d)"
config_dir: "$SCRIPT_PATH/config"
verify: false
realm: test
EOF

rdynacloak serve
