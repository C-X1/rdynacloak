"""Test the ansible executor."""

from rpycihelpers.foldered_test import FolderedPyTest

from rdynacloak.ansible_executor import AnsibleExecutor, list_ansible_keycloak_modules


class AnsibleExecutorTest(FolderedPyTest):
    """Test the KeycloakConfigApplicator."""

    @staticmethod
    def test_list_keycloak_modules() -> None:
        """Test listing keycloak modules."""
        result = list_ansible_keycloak_modules()

        assert isinstance(result, list)
        for name in ("role", "user_federation", "client_rolemapping", "group", "user"):
            assert name in result, result

        assert result[0] == "user_federation"

    @staticmethod
    def test_simple_module_call() -> None:
        """Test simple module call."""
        aexec = AnsibleExecutor()

        result = aexec.run_ansible_module(
            "ansible_collections.community.general.plugins.modules.keycloak_user",
            {
                "auth_username": "user",
                "auth_client_id": "admin-cli",
                "auth_password": "password",
                "username": "test_user",
            },
        )

        assert result["failed"]
