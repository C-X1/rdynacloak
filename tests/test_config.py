"""Test the config."""

from unittest.mock import patch

import pytest
from rpycihelpers.files import create_file
from rpycihelpers.foldered_test import FolderedPyTest

from rdynacloak.config import RDynacloakConfiguration, read_configuration


class ConfigTest(FolderedPyTest):
    """Test the configuration."""

    @staticmethod
    def test_read_configuration() -> None:
        """Test read configuration."""
        config_path = "config.css"
        mp = pytest.MonkeyPatch()
        mp.setenv("RDYNACLOAK_CONFIG_PATH", config_path)
        config_content = """
        keycloak_url: "https://localhost:9090"
        auth_username: admin
        auth_password: testpw
        auth_realm: master
        auth_client_id: test_client

        config_dir: "/sidecardir"

        realm: "dynamic"

        verify: False

        timeout: 10

        css_file: /test.css
        """
        create_file(config_path, config_content)

        result = read_configuration()

        assert isinstance(result, RDynacloakConfiguration)
        assert result.config_dir == "/sidecardir"
        assert result.realm == "dynamic"
