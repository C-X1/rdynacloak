"""Test the config applicator."""

import yaml
from pydantic import ValidationError
from rpycihelpers.files import create_dir, create_file
from rpycihelpers.foldered_test import FolderedPyTest

from rdynacloak.config_file import ConfigFile


class ConfigFileTest(FolderedPyTest):
    """Test the Config file generation."""

    @staticmethod
    def test_config_file_class() -> None:
        """Test loading config file from yaml."""
        content = """
            service_id: "my service"
            user_federations:
                - f: 1
                - f: 2
                - f: 3
            """
        yaml_content = yaml.safe_load(content)

        result = ConfigFile(**yaml_content)

        assert isinstance(result, ConfigFile)
        assert isinstance(result.user_federations, list)
        assert len(result.user_federations) == 3

    @staticmethod
    def test_read_config_file() -> None:
        """Test reading a config file."""
        good_file = ConfigFile(**yaml.safe_load("service_id: a12345\n"))

        create_dir("config")
        create_file("config/t1.yaml", "service_id: a12345")
        create_file("config/t2.yAml", "service_id: a12345")
        create_file("config/t3.Yml", "service_id: a12345")

        result = [
            ConfigFile.read_config_file("config/t1.yaml"),
            ConfigFile.read_config_file("config/t2.yAml"),
            ConfigFile.read_config_file("config/t3.Yml"),
        ]

        assert result == [
            good_file,
            good_file,
            good_file,
        ]

    @staticmethod
    def test_load_from_directory() -> None:
        """Test reading a config file."""
        create_dir("config")
        create_dir("config/dir_yaml_ext.yaml")
        create_file("config/t1.yaml", "Murx: 123")
        create_file("config/t2.yAml", "service_id: a12345")

        result = ConfigFile.load_from_directory("config")

        assert isinstance(result["dir_yaml_ext.yaml"], IsADirectoryError)
        assert isinstance(result["t1.yaml"], ValidationError)
        assert isinstance(result["t2.yAml"], ConfigFile)
