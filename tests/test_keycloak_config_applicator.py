"""Test the config applicator."""

from unittest.mock import patch

import pytest
import yaml
from rpycihelpers.files import create_dir, create_file
from rpycihelpers.foldered_test import FolderedPyTest

from rdynacloak.config import RDynacloakConfiguration
from rdynacloak.keycloak_config_applicator import (
    ConfigFile,
    KeycloakConfigApplicator,
    list_configurations,
)
from rdynacloak.keycloak_token_handler import KeycloakTokenHandler


class KeycloakConfigApplicatorTest(FolderedPyTest):
    """Test the KeycloakConfigApplicator."""

    @staticmethod
    def test_list_configurations() -> None:
        """Test listing ansible keycloak and custom modules."""
        result = list_configurations()

        assert isinstance(result, list)
        for name in (  # noqa: WPS352
            "role",
            "user_federation",
            "client_rolemapping",
            "group",
            "user",
            "flow",
        ):
            assert name in result

    @staticmethod
    def test_is_read_good_general() -> None:
        """Test is_service_status function when none is true."""
        test_object = KeycloakConfigApplicator(RDynacloakConfiguration())
        test_object._service_status["service_a"] = True
        test_object._service_status["service_c"] = False
        test_object._service_status["general"] = True

        result = [
            test_object.is_service_ready("service_a"),
            test_object.is_service_ready("service_b"),
            test_object.is_service_ready("service_c"),
            test_object.is_service_ready("general"),
        ]

        assert result == [True, False, False, True]

    @staticmethod
    def test_is_read_bad_general() -> None:
        """Test is_service_ready function when none is false."""
        test_object = KeycloakConfigApplicator(RDynacloakConfiguration())
        test_object._service_status["service_a"] = True
        test_object._service_status["service_c"] = False
        test_object._service_status["general"] = False

        result = [
            test_object.is_service_ready("service_a"),
            test_object.is_service_ready("service_b"),
            test_object.is_service_ready("service_c"),
            test_object.is_service_ready("general"),
        ]

        assert result == [False, False, False, False]

    @staticmethod
    def test_reset_config_realm_master() -> None:
        """Test reset config."""
        test_cls = KeycloakConfigApplicator(RDynacloakConfiguration())
        test_cls._config.realm = "master"
        test_cls._service_status = {"a": True}

        with patch.object(KeycloakConfigApplicator, "delete_pod") as mock:
            test_cls.reset_config()
            test_cls.reset_config()

            assert mock.call_count == 2
            assert test_cls._service_status == {}

    @staticmethod
    def test_reset_config_realm_other() -> None:
        """Test reset config."""
        test_cls = KeycloakConfigApplicator(RDynacloakConfiguration())
        test_cls._config.realm = "other"
        test_cls._service_status = {"a": True}

        with patch.object(KeycloakConfigApplicator, "delete_realm") as mock:
            test_cls.reset_config()
            test_cls.reset_config()

            assert mock.call_count == 2
            assert test_cls._service_status == {}

    @staticmethod
    @pytest.mark.skip("Does create config in project dir, why?")
    def test_load_configurations() -> None:
        """Test loading config files."""
        config = RDynacloakConfiguration()
        config.config_dir = "config"
        test_cls = KeycloakConfigApplicator(config)
        create_dir("config")
        service1 = """
        service_id: service1
        users:
            - name: Test1
            - name: Test2
        """
        service2 = """
        service_id: service2
        clients:
            - name: Test1
            - name: Test2
        """
        create_dir("config/not_a_file.yaml")
        create_file("config/service1.Yml", service1)
        create_file("config/service2.yaml", service2)
        create_file("config/service3.yaml", "bad file.")
        create_file("config/service4.yaml", "good_yaml_but_bad: True")
        create_file("config/service5.txt", "service_id: nope")

        result = test_cls.load_configuration_files()

        assert isinstance(result, dict)
        assert len(result) == 2

    @staticmethod
    def test_apply_config_from_file() -> None:
        """Test apply configs from ConfigFile object."""
        test_cls = KeycloakConfigApplicator(RDynacloakConfiguration())
        config_file0 = ConfigFile(service_id="A")
        config_file1 = ConfigFile(service_id="B")
        config_file0.users = [
            {"name": "Test1"},
            {"name": "Test2"},
        ]
        config_file1.flows = [
            {"name": "Test3"},
            {"name": "Test4"},
            {"name": "Test5"},
        ]

        with (  # noqa: WPS316
            patch.object(KeycloakConfigApplicator, "apply_ansible_config") as m_ansible,
            patch.object(KeycloakConfigApplicator, "apply_custom_config") as m_custom,
            patch.object(
                KeycloakTokenHandler, "get_access_token", return_value="token"
            ) as m_token,
        ):
            test_cls.apply_config_from_file("user", config_file0)
            test_cls.apply_config_from_file("flow", config_file1)

            assert m_ansible.call_count == 2
            assert m_custom.call_count == 3
            assert m_token.call_count == 5
            assert test_cls._applied_count == 5

    @staticmethod
    def test_apply_config_from_file_ni() -> None:
        """Test apply configs with not implemented error."""
        test_cls = KeycloakConfigApplicator(RDynacloakConfiguration())
        config_file0 = ConfigFile(service_id="C")
        config_file0.flows = [
            {"name": "Test3"},
            {"name": "Test4"},
            {"name": "Test5"},
        ]

        with (  # noqa: WPS316
            patch.object(
                KeycloakConfigApplicator,
                "apply_custom_config",
                side_effect=NotImplementedError("NI"),
            ) as m_custom,
            patch.object(
                KeycloakTokenHandler, "get_access_token", return_value="token"
            ) as m_token,
        ):
            test_cls.apply_config_from_file("flow", config_file0)

            assert test_cls._applied_count == 3
            assert m_custom.call_count == 1
            assert m_token.call_count == 1
            assert not test_cls._service_status["C"]

    @staticmethod
    def test_apply_config_from_file_ex() -> None:
        """Test apply configs with other error."""
        test_cls = KeycloakConfigApplicator(RDynacloakConfiguration())
        config_file0 = ConfigFile(service_id="D")
        config_file0.flows = [
            {"name": "Test3"},
            {"name": "Test4"},
            {"name": "Test5"},
        ]

        with (  # noqa: WPS316
            patch.object(
                KeycloakConfigApplicator,
                "apply_custom_config",
                side_effect=ValueError("value"),
            ) as m_custom,
            patch.object(
                KeycloakTokenHandler, "get_access_token", return_value="token"
            ) as m_token,
        ):
            test_cls.apply_config_from_file("flow", config_file0)

            assert test_cls._applied_count == 3
            assert m_custom.call_count == 3
            assert m_token.call_count == 3
            assert not test_cls._service_status["D"]

    @staticmethod
    def test_apply_custom_config() -> None:
        """Test applying custom config."""
        test_cls = KeycloakConfigApplicator(RDynacloakConfiguration())

        with (  # noqa: WPS316
            patch.object(
                KeycloakConfigApplicator,
                "apply_custom_flow",
            ) as m_custom,
        ):
            test_cls.apply_custom_config("flow", {"test": 1234})

            m_custom.assert_called_once()

    @staticmethod
    def test_init_update_status() -> None:
        """Test apply configs from ConfigFile object."""
        test_cls = KeycloakConfigApplicator(RDynacloakConfiguration())
        test_cls._applied_count = 123
        config_file0 = ConfigFile(service_id="A")
        config_file1 = ConfigFile(service_id="B")
        config_file0.users = [
            {"name": "Test1"},
            {"name": "Test2"},
        ]
        config_file1.flows = [
            {"name": "Test3"},
            {"name": "Test4"},
            {"name": "Test5"},
        ]

        test_cls.init_update_status({"A": config_file0, "B": config_file1})

        assert test_cls._overall_count == 5
        assert "A" in test_cls._service_status_update
        assert "B" in test_cls._service_status_update
        assert test_cls._applied_count == 0

    @staticmethod
    def test_init_run() -> None:
        """Test apply run configs."""
        config = RDynacloakConfiguration()
        config.config_dir = "config"
        test_cls = KeycloakConfigApplicator(config)
        service1 = """
        service_id: service1
        users:
            - name: Test1
            - name: Test2
        """
        service2 = """
        service_id: service2
        clients:
            - name: Test1
            - name: Test2
        """
        create_dir("config")
        create_file("config/service1.Yml", service1)
        create_file("config/service2.yaml", service2)
        create_file("config/bad.yaml", "bad bad bad")

        with (  # noqa: WPS316
            patch.object(
                KeycloakConfigApplicator,
                "apply_config_from_file"
            ) as m_apply,
            patch.object(
                KeycloakConfigApplicator,
                "reset_config",
            ) as m_reset,
        ):
            test_cls.apply_configs(reset=True)
            test_cls.apply_configs(reset=False)

            assert m_reset.call_count == 1
            assert m_apply.call_count == 4

    @staticmethod
    @pytest.mark.skip("Interferes with other tests, why?")
    def test_apply_custom_config_not_found() -> None:
        """Test applying custom config function missing."""
        config = RDynacloakConfiguration()
        config.config_dir = "config"
        create_dir("config")
        test_cls = KeycloakConfigApplicator(config)

        with pytest.raises(NotImplementedError) as error:
            test_cls.apply_custom_config("na", {"test": 1234})
