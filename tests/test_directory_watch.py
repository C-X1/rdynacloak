"""Test directory watch."""

import os
import shutil
import time

from rpycihelpers.files import create_dir, create_file
from rpycihelpers.foldered_test import FolderedPyTest

from rdynacloak.directory_watch import DirectoryWatcher


class DirectoryWatchTest(FolderedPyTest):
    """Test directory watch."""

    @staticmethod
    def test_directory_watcher() -> None:  # noqa: WPS213
        """Test counting changes."""
        create_dir("watched")
        create_file("watched/del1.yaml", "1234")
        create_file("watched/del2.yaml", "1234")
        create_file("watched/move.yaml", "1234")
        watcher = DirectoryWatcher("watched")
        wait_time_for_watcher = 0.5
        expected_counts = {
            "created": 2,
            "deleted": 2,
            "moved": 1,
            "modified": 2,
        }
        time.sleep(wait_time_for_watcher)
        watcher.start()
        create_dir("watched/test_dir")
        create_file("watched/t1.yaml", "12345")
        create_file("watched/t2.yaml", "12345")
        create_file("watched/other.txt", "1234")
        with open("watched/t2", "w") as cfile:
            cfile.write("1234")
        os.remove("watched/del1.yaml")
        os.remove("watched/del2.yaml")
        shutil.move("watched/move.yaml", "watched/moved.yaml")
        time.sleep(wait_time_for_watcher)
        watcher.stop()
        result = watcher.obtain_counts()
        result1 = watcher.obtain_counts_and_reset()
        result2 = watcher.obtain_counts_and_reset()

        assert result == expected_counts
        assert result1 == expected_counts
        assert result2 == {
            "created": 0,
            "deleted": 0,
            "moved": 0,
            "modified": 0,
        }
